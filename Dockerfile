FROM python:3.6

RUN mkdir /app
WORKDIR /app
COPY ./requirements.txt /app

RUN pip install -r requirements.txt
COPY . /app

CMD ["python", "app/manage.py", "runserver", "0.0.0.0:8000"]

EXPOSE 8000


